#!/bin/bash

bundle install

bundle exec erb config/vpc_settings.yaml.erb > config/vpc_settings.yaml

bundle exec cfndsl -p -v -y config/vpc_settings.yaml -o output/vpc_cloudformation.json vpc.rb 
