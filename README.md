# README #

# Production Use #

Run this from https://jenkins.cloudservices.huit.harvard.edu/view/Automation%20Jobs/job/hcdo-vpc-cloudformation/



## Development Setup ##

To deploy to the Jenkins system any changes and ensure they are merged into release after proper reviews.

The next run will refresh the Jenkins code from release on the master repository.

### RVM Setup ###

**IMPORTANT: DO NOT RUN WITH SUDO**

1. Import RVM GPG Key and run 
    
        "\curl -sSL https://get.rvm.io | bash -s stable" - Key located at http://rvm.io/rvm/install

1. Re-source your shell (bash/zsh) profiles

### Ruby Setup ###

    rvm install ruby-2.3.3
    rvm use 2.3.3@global
    gem install bundler
    rvm use 2.3.3
    rvm gemset create cfndsl
    rvm use 2.3.3@cfndsl
    bundle install

### Running the tool for Development ###

## Setup ##

Use the .yaml.example to setup your yaml file in the config directory and copy it to .yaml.  Pay special attention to the comments

## Running the tool ##

Ensure you in the cfndsl gemset.  (rvm use 2.3.3-cfndsl)

    bundle exec cfndsl -v vpc.rb -y config/vpc_settings.yaml -p > output/vpc_cloudformation.json

Then upload this to the CloudFormation UI and execute.

## TODO ##

1. Add Pre-Compile Step to get AZ's