require 'ipaddress'
require 'cfndsl'




# Begin the CloudFormation Creation
CloudFormation {
  Description 'Creates an AWS VPC Subnets and VGW'

  # Master Variables to be used later declared here for scope reasons
  az_count = nil
  elb_private_standard_subnets = []
  elb_private_level4_subnets = []
  app_private_standard_subnets = []
  app_private_level4_subnets = []
  db_private_standard_subnets = []
  db_private_level4_subnets = []

  elb_net_count = nil
  app_net_count = nil
  db_net_count = nil
  elb_prefix = nil
  app_prefix = nil
  db_prefix = nil

  # This is the lowest CIDR which is required
  subnetting_cidr = 26

  # Sanity Checks for items which are not defined
  abort('ERROR: VPC CIDR Not defined in yaml') unless defined? VpcCidr
  abort('ERROR: Availability zones not defined in yaml') unless defined? AccountAZs
  abort('ERROR: HUIT Asset ID is not defined in yaml') unless defined? HuitAssetId

  # Parameters to be used in the template
  # These are presented to the end-user in the interface, but are informational only
  # They are to be defined in config/vpc_settings.yaml.erb via environmental variables
  Parameter('GroupName') {
    Type 'String'
    Description "Group Name for VPC (e.g 'admints')"
    MinLength '3'
    MaxLength '18'
    AllowedPattern '([a-zA-Z]+)+(?!(dev|prod|stage|sta))'
    ConstraintDescription 'Must be the group name only, do not include the environment'
    Default GroupName
  }
  Parameter('VpcEnvironment') {
    Type 'String'
    Description "Environment for the VPC (e.g. 'dev')"
    MinLength '3'
    MaxLength '5'
    AllowedPattern '(dev|stage|test|prod)'
    ConstraintDescription "Must be the environment matching one of 'dev, test, stage, prod'"
    Default VpcEnvironment
  }
  Parameter('HuitAssetId') {
    Type 'String'
    Description 'Slash Asset ID number assigned by ICAPS'
    MinLength '2'
    MaxLength '4'
    AllowedPattern '([1-9][0-9]{3}|-1)'
    ConstraintDescription 'Must be a 4 digit number'
    Default HuitAssetId
  }
  Parameter('AccountType') {
    Type 'String'
    Description 'Account Type, one of: standard, level4, bcdr'
    AllowedPattern '(standard|level4|bcdr)'
    ConstraintDescription "Must match 'standard' or 'level4' or 'bcdr'"
    Default AccountType
  }
  Parameter('AccountAZs') {
    Type 'CommaDelimitedList'
    Description 'Account Availability Zones to be used'
    Default AccountAZs.join(', ')
  }
  Parameter('VpcCidr') {
    Type 'String'
    Description 'The allocated CIDR block for the VPC'
    AllowedPattern '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$'
    ConstraintDescription "Must be a valid CIDR block"
    Default VpcCidr
  }


  cf_vpc_name_construct = GroupName.capitalize + VpcEnvironment.capitalize
  res_vpc_name_construct = "#{GroupName.downcase}-#{VpcEnvironment.downcase}"
  availability_zones = AccountAZs.to_a



  # START OF SUBNETTING
  #VpcCidr = '100.64.0.0/20' # FOR TESTING ONLY
  vpc_super_network = IPAddress(VpcCidr)


  # The numbers here are the number of subnetting_cidr's which are required to make a
  # network of their required size

  case vpc_super_network.prefix
    when 20
      az_count = 3
      elb_prefix = 25
      elb_net_count = 2
      app_prefix = 24
      app_net_count = 4
      db_prefix = 26
      db_net_count = 1
    when 21
      az_count = 3
      elb_prefix = 25
      elb_net_count = 2
      app_prefix = 25
      app_net_count = 2
      db_prefix = 26
      db_net_count = 1

    when 22
      az_count = 2
      elb_prefix = 25
      elb_net_count = 2
      app_prefix = 26
      app_net_count = 1
      db_prefix = 26
      db_net_count = 1
  end

  # Break it into the lowest possible CIDR required
  vpc_subnetting_networks = vpc_super_network.subnet(subnetting_cidr)

  # Create the ELB Standard networks
  az_count.times do
    networks = vpc_subnetting_networks.shift(elb_net_count)
    agg_net = IPAddress::IPv4::summarize(*networks).first
    if agg_net.prefix == elb_prefix
      elb_private_standard_subnets << agg_net
    else
      raise "Expected Prefix of #{elb_prefix}, got #{agg_net.prefix}"
    end
  end

  # Create the ELB Level4 networks
  az_count.times do
    networks = vpc_subnetting_networks.shift(elb_net_count)
    agg_net = IPAddress::IPv4::summarize(*networks).first
    if agg_net.prefix == elb_prefix
      elb_private_level4_subnets << agg_net
    else
      raise "Expected Prefix of #{elb_prefix}, got #{agg_net.prefix}"
    end
  end

  # Create the App Standard networks
  az_count.times do
    networks = vpc_subnetting_networks.shift(app_net_count)
    agg_net = IPAddress::IPv4::summarize(*networks).first
    if agg_net.prefix == app_prefix
      app_private_standard_subnets << agg_net
    else
      raise "Expected Prefix of #{app_prefix}, got #{agg_net.prefix}"
    end
  end

  # Create the App Level4 networks
  az_count.times do
    networks = vpc_subnetting_networks.shift(app_net_count)
    agg_net = IPAddress::IPv4::summarize(*networks).first
    if agg_net.prefix == app_prefix
      app_private_level4_subnets << agg_net
    else
      raise "Expected Prefix of #{app_prefix}, got #{agg_net.prefix}"
    end
  end

  # Create the DB Standard networks
  az_count.times do
    networks = vpc_subnetting_networks.shift(db_net_count)
    agg_net = IPAddress::IPv4::summarize(*networks).first
    if agg_net.prefix == db_prefix
      db_private_standard_subnets << agg_net
    else
      raise "Expected Prefix of #{db_prefix}, got #{agg_net.prefix}"
    end
  end

  # Create the DB Level4 networks
  az_count.times do
    networks = vpc_subnetting_networks.shift(db_net_count)
    agg_net = IPAddress::IPv4::summarize(*networks).first
    if agg_net.prefix == db_prefix
      db_private_level4_subnets << agg_net
    else
      raise "Expected Prefix of #{db_prefix}, got #{agg_net.prefix}"
    end
  end


  # Store the resulting arrays from the breakup into the hash structure
  private_subnets['elb']['networks'] = elb_private_standard_subnets
  private_subnets['elb_level4']['networks'] = elb_private_level4_subnets

  private_subnets['app']['networks'] = app_private_standard_subnets
  private_subnets['app_level4']['networks'] = app_private_level4_subnets

  private_subnets['db']['networks'] = db_private_standard_subnets
  private_subnets['db_level4']['networks'] = db_private_level4_subnets

  # END OF SUBNETTING

  #Create the VPC
  VPC(:VPC) {
    EnableDnsSupport true
    EnableDnsHostnames true
    CidrBlock VpcCidr
    addTag('Name', "#{res_vpc_name_construct}-#{AccountType.downcase}-vpc")
    addTag('Environment', Ref('VpcEnvironment'))
  }

  Output("VpcID"){
    Value Ref(:VPC)
  }

  Output("VpcCidrBlock"){
    Value Ref(:VpcCidr)
  }

  #Creation the DHCP Options Set
  DHCPOptions(:DHCPOptions){
    DomainNameServers [vpc_dhcp_settings['dns_nameservers'].join(', ')]
    NtpServers [vpc_dhcp_settings['ntp_servers'].join(', ')]
    addTag('Name', "#{res_vpc_name_construct}-dhcpoptions")
    addTag('Environment', Ref('VpcEnvironment'))
  }

  # Associate the DHCP Options Set to the VPC
  VPCDHCPOptionsAssociation(:VPCDHCPOptionsAssociation) {
    DhcpOptionsId Ref(:DHCPOptions)
    VpcId Ref(:VPC)
  }

  #Create the VGW for the VPC
  VPNGateway(:VirtualPrivateNetworkGateway) {
    Type 'ipsec.1'
    addTag('Name', "#{res_vpc_name_construct}-vgw")
  }

  # Attach the VGW to the VPC
  VPCGatewayAttachment(:VPNGatewayAttachment) {
    DependsOn :VirtualPrivateNetworkGateway
    VpcId Ref(:VPC)
    VpnGatewayId Ref(:VirtualPrivateNetworkGateway)
  }

  # Create single private route table and enable Route Prop and set VGW Dependancy
  RouteTable("#{cf_vpc_name_construct}PrivateRoute") {
    VpcId Ref(:VPC)
    addTag('Name', "#{res_vpc_name_construct}-private-rt")
  }
  Output("PrivateRoute"){
    Value Ref("#{cf_vpc_name_construct}PrivateRoute")
  }
  VPNGatewayRoutePropagation("#{cf_vpc_name_construct}Prop"){
    VpnGatewayId Ref(:VirtualPrivateNetworkGateway)
    RouteTableIds [Ref("#{cf_vpc_name_construct}PrivateRoute")]
    DependsOn :VPNGatewayAttachment
  }
  VPCGatewayAttachment(:VPNGatewayAttachment) {
    DependsOn :VirtualPrivateNetworkGateway
    VpcId Ref(:VPC)
    VpnGatewayId Ref(:VirtualPrivateNetworkGateway)
  }

  az_count.times { |i|

  i = 1
    private_subnets.each { |name,vals|
      counter = 0

    az_count.times do
      subnet_type = vals['type']
      route_type = vals['scope']
      security_type = vals['security']
      address = vals['networks'][counter].address
      prefix = vals['networks'][counter].prefix
      cf_subnet_name = "#{cf_vpc_name_construct}#{security_type.capitalize}#{subnet_type.capitalize}#{route_type.capitalize}Subnet#{counter}"
      tag_subnet_name = "#{res_vpc_name_construct}-#{security_type}-#{subnet_type}-#{route_type}-#{availability_zones[counter].split('-')[2]}-#{i}"

      route_private_table = "#{cf_vpc_name_construct}PrivateRoute"
      route_table_assoc = route_private_table + "#{subnet_type.capitalize}#{route_type.capitalize}#{security_type.capitalize}Assoc#{counter}"

      Subnet(cf_subnet_name) {
        VpcId Ref(:VPC)
        CidrBlock "#{address}/#{prefix}"
        AvailabilityZone availability_zones[counter]
        addTag("Name", tag_subnet_name)
      }
      SubnetRouteTableAssociation(route_table_assoc) {
        DependsOn [route_private_table, cf_subnet_name]
        SubnetId Ref(cf_subnet_name)
        RouteTableId Ref(route_private_table)
      }
      Output("#{security_type.capitalize}#{subnet_type.capitalize}#{route_type.capitalize}Subnet#{counter}") {
        Value Ref(cf_subnet_name)
      }
      counter += 1
      end #end az_count
    }
    i += 1
  } # End AZ Iterator
} #End CloudFormation
